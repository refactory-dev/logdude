module Logdude
  class EntriesController < ApplicationController
    def index
      render json: Logdude::Models::Entry.all(params[:model_type], params[:model_id].to_i)
    end
  end
end
