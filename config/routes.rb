Rails.application.routes.draw do
  get 'logdude/entries/:model_type/:model_id' => 'logdude/entries#index', :as => 'logs_dude',
                                                  :constraints => {:model_type => /\w+/, :model_id => /\d+/}
end
