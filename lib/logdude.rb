require 'active_record'
require 'rails'

require 'logdude/version'
require 'logdude/railtie' if defined?(Rails)
require 'logdude/helpers/configuration'
require 'logdude/helpers/show'
require 'logdude/engine'
require 'logdude/activerecord_concerns'
require 'logdude/controller_concerns'

module Logdude
  extend Configuration

  define_setting :port, 27017
  define_setting :host, '127.0.0.1'
  define_setting :db_name, 'logdudedb'
  define_setting :username
  define_setting :password
  define_setting :db

end
