require 'logdude/models/entry'

module Logmedude
  extend ActiveSupport::Concern

  included do
    before_create :log
    before_update :log
  end

  private

  def log
    Logdude::Models::Entry.log(self)
  end
end
