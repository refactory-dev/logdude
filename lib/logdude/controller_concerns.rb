module InjectChangerDude
  extend ActiveSupport::Concern

  included do
    append_before_action :_inject_owner
  end

  private

  def _inject_owner
    changer = current_employee # dirty hardcoded devise method
    ActiveRecord::Base.class.send(:define_method, 'changer', proc {changer.id})
  end
end
