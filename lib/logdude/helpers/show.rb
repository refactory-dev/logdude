require 'action_view'

require 'logdude/models/entry'

module Logdude
  module Helper
    include ActionView::Helpers::TagHelper

    def show_me_dude(model_type, model_id)
      render(partial: 'helpers/show', locals: {model_type: model_type, model_id: model_id})
    end
  end
end
