require 'mongo'

module Logdude
  module Models
    class Entry
      attr_accessor :created_at, :model_id, :model_type, :before, :after, :user_id, :metadata

      def self.all(model_type=nil, model_id=nil)
        client = Logdude.db

        if model_id && model_type
          client[:logs].find(model_id: model_id, model_type: model_type)
        else
          client[:logs].find
        end
      end

      def self.log(instance)
        client = Logdude.db
        begin
        client[:logs].insert_one({
          created_at: DateTime.now,
          model_id: instance.id,
          model_type: instance.class.name,
          before: instance.attributes.keys.collect {|k| {"#{k}" => instance.send("#{k}_was")}},
          after: instance.attributes.keys.collect {|k| {"#{k}" => instance.send("#{k}")}},
          user_id: ActiveRecord::Base.class.method_defined?(:changer) ? ActiveRecord::Base.class.changer : ''
        }) unless Rails.env == 'test'
        rescue
          p "error"
        end
      end
    end
  end
end
