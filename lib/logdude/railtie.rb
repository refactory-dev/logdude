require 'logdude/helpers/show'

module Logdude
  class Railtie < Rails::Railtie
    initializer "logdude.helpers.show" do
      ActionView::Base.send :include, Helper
    end
  end
end
