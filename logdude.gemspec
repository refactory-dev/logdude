# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'logdude/version'

Gem::Specification.new do |spec|
  spec.name          = "logdude"
  spec.version       = Logdude::VERSION
  spec.authors       = ["pawel@buchowski@gmail.com"]
  spec.email         = ["pawel.buchowski@gmail.com"]
  spec.summary       = 'Simply mongodb action logger'
  spec.description   = 'Simply logger which use own mongodb to store logs. Comes with helper to list all logs.'
  spec.homepage      = "http://cottonbee.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "sqlite3-ruby", "~> 1.3"
  spec.add_development_dependency "rspec", "~> 3.3"

  spec.add_dependency "activerecord"
  spec.add_dependency "rails"
  spec.add_dependency "mongo"
end
