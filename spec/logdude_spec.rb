require 'spec_helper'
require 'mongo'

require 'support/models'


RSpec.describe Logdude do
  before(:each) do
    @mongo = Mongo::Client.new(["#{Logdude.host}:#{Logdude.port}"],
                                database: Logdude.db_name,
                                user: Logdude.username,
                                password: Logdude.password)
  end

  after(:each) do
    @mongo[:logs].find.delete_many
  end

  describe 'logdude logger' do
    it 'should create log after model creation' do
      post = Post.create!
      cnt = @mongo[:logs].find(model_id: post.id).count

      expect(cnt).to eq 1
    end

    it 'should create log after model update' do
      post = Post.create!
      post.update name: 'test'
      cnt = @mongo[:logs].find(model_id: post.id).count

      expect(cnt).to eq 2
    end

    it 'should store changed data on model update' do
      post = Post.create! name: 'test'
      post.update name: 'test2'

      logs = @mongo[:logs].find(model_id: post.id).sort(created_at: -1).limit(1)
      puts '----'
      logs.each do |l|
        l[:before].each do |h|
          if h.has_key?('name')
            expect(h['name']).to eq 'test'
          end
        end
      end
    end
  end

end
